#!/bin/bash 



action=$1

path=$2

filename=$3

addcontent=$4

arg5=$5

#echo "$action $path $filename"



if [ $action == "addDir" ]

then	

	if [ -d "$path/$filename" ]

	then

		echo "Directory $filename already exists."

	else	

		mkdir -p $path/$filename

		echo "Directory $filename created Successfully."

	fi

fi



if [ $action == "deleteDir" ]

then 

	if [ -d "$path/$filename" ]

     	then

		rmdir $path/$filename

		echo "Directory $filename deleted Successfully."

	else

        echo "Directory $filename does not exist."

	fi

fi



if [ $action == "listAll" ]

then

	ls -ll $path

fi



if [ $action == "listDirs" ]

then

       find $path -maxdepth 1 -type d

fi



if [ $action == "listFiles" ]

then

       find $path -maxdepth 1 -type f

fi



if [ $action == "addFile" ]

then

	if [ -f "$path/$filename" ]

	then

		echo "File $filename already exists."

	elif [ -z $addcontent ]

	then	

		touch $path/$filename

		echo "File $filename created Successfully."

	else

		echo "$addcontent" > $path/$filename

		#echo $addcontent

		echo "File $filename created Successfully."

	fi

fi



if [ $action == "addContentToFile" ]

then	

        echo "$addcontent" >> $path/$filename

        echo "Content appended to file $filename Successfully."

fi



if [ $action = "addContentToFileBegining" ]

then

	x=`echo ${addcontent}; cat $path/$filename`

	echo "$x" > $path/$filename

        #echo "$addcontent" >> $path/$filename

        echo "Content added in the begining to file $filename Successfully."

fi



if [ $action == "showFileBeginingContent" ]

then

	head -n $addcontent $path/$filename

	echo "Showing first $addcontent lines of file $filename"

fi



if [ $action == "showFileEndContent" ]

then

	tail -n $addcontent $path/$filename

        echo "Showing last $addcontent lines of file $filename"

fi



if [ $action == "showFileContentAtLine" ]

then

        head -$addcontent $path/$filename|tail -$addcontent

        echo "Showing line no. $addcontent of file $filename"

fi



if [ $action == "showFileContentForLineRange" ]

then

        head -$arg5 $path/$filename|tail -$addcontent

        echo "Showing line range from $addcontent to line $arg5 of file $filename"

fi



if [ $action == "moveFile" ]

then

        mv $path $filename

        echo "File moved to path $filename Successfully"

fi



if [ $action == "copyFile" ]

then

        cp -r $path $filename

        echo "File copied to path $filename Successfully"

fi



if [ $action == "clearFileContent" ]

then

        echo > $path/$filename

        echo "Content of file $filename cleared Successfully"

fi



if [ $action == "deleteFile" ]

then

        if [ -f "$path/$filename" ]

        then

                rm $path/$filename

                echo "File $filename deleted Successfully."

        else

        echo "File $filename does not exist."

        fi

fi




